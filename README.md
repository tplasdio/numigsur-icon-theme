# What is this?

This is an icon theme based on Numix-circle that I no longer find anywhere. I'm unaware if the original project changed its name so I thought I might as well archive it, maybe make and fill some missing icons on my own. If you know what happened to it, do let me know.
